package calc.ferdous.mycalcv2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnadd,btnsub,btnmul,btndiv,btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0,btnpoint,btnequal,btnhis,btnac,btnms,btnclear,btnmc,btnmr,btnmplus,btnmminus;

    EditText txt1,txt2;

    TextView txtresult;

    boolean cal_flag=true;

    //String data1,data2;








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("MainActivity","Oncreate");



            buttonAction();






    }

   @Override
    protected void onStart() {
        super.onStart();

       Log.d("MainActivity","onStart");

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("MainActivity","onResume");

    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d("MainActivity","onPause");

    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d("MainActivity","onStop");

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.d("MainActivity","onRestart");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d("MainActivity","onDestroy");

    }


    private void buttonAction(){


        btnadd = (Button)findViewById(R.id.btnadd);
        btnsub = (Button)findViewById(R.id.btnsub);
        btnmul = (Button)findViewById(R.id.btnmul);
        btndiv = (Button)findViewById(R.id.btndiv);
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 =  (Button) findViewById(R.id.btn4);
        btn5 =  (Button) findViewById(R.id.btn5);
        btn6 =  (Button) findViewById(R.id.btn6);
        btn7 =  (Button) findViewById(R.id.btn7);
        btn8 =  (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnpoint =(Button) findViewById(R.id.btnpoint);
        btnequal =(Button) findViewById(R.id.btnequal);
        btnhis =(Button) findViewById(R.id.btnhis);
        btnac =(Button) findViewById(R.id.btnac);
        btnms =(Button) findViewById(R.id.btnms);
        btnclear =(Button) findViewById(R.id.btnclear);
        btnmc =(Button) findViewById(R.id.btnmc);
        btnmr =(Button) findViewById(R.id.btnmr);
        btnmplus =(Button) findViewById(R.id.btnmplus);
        btnmminus =(Button) findViewById(R.id.btnmminus);

        txtresult= (TextView) findViewById(R.id.txtresult);

        txt1= (EditText) findViewById(R.id.txt1);
        //txt2= (EditText) findViewById(R.id.txt2);


        btnadd.setOnClickListener(this);
        btnsub.setOnClickListener(this);
        btnmul.setOnClickListener(this);
        btndiv.setOnClickListener(this);
        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnpoint.setOnClickListener(this);
        btnequal.setOnClickListener(this);
        btnac.setOnClickListener(this);
        btnms.setOnClickListener(this);
        btnmr.setOnClickListener(this);
        btnmplus.setOnClickListener(this);
        btnmminus.setOnClickListener(this);
        btnmc.setOnClickListener(this);
        btnclear.setOnClickListener(this);



    }


    @Override
    public void onClick(View view) {

        if (view.getId()==R.id.btnadd){

            if(cal_flag==false){

                txt1.setText(txt1.getText()+"+");
                cal_flag=true;
            }else{

                Toast.makeText(this,"Check input",Toast.LENGTH_LONG).show();
            }

        /*
        String data1 = txt1.getText().toString();
        String data2 = txt2.getText().toString();

        Intent i = new Intent(getApplicationContext(),Calculate.class);
        i.putExtra("num1",data1);
        i.putExtra("num2",data2);
        i.putExtra("flag","+");
        startActivityForResult(i,1);

        */



        // int result = Integer.parseInt(data1) + Integer.parseInt(data2);

        //txtresult.setText(String.valueOf(result));
        }else if(view.getId()==R.id.btnsub){

            if(cal_flag==false){

                txt1.setText(txt1.getText()+"-");
                cal_flag=true;
            }else{

                Toast.makeText(this,"Check input",Toast.LENGTH_LONG).show();
            }

            /*
            String data1 = txt1.getText().toString();
            String data2 = txt2.getText().toString();

            Intent i = new Intent(getApplicationContext(),Calculate.class);
            i.putExtra("num1",data1);
            i.putExtra("num2",data2);
            i.putExtra("flag","-");
            startActivityForResult(i,2);

            */
            //int result = Integer.parseInt(data1) - Integer.parseInt(data2);

            //txtresult.setText(String.valueOf(result));


        }else if(view.getId()==R.id.btnmul){



            if(cal_flag==false) {

                txt1.setText(txt1.getText() + "*");
                cal_flag = true;
            }else{

                Toast.makeText(this,"Check input",Toast.LENGTH_LONG).show();
            }

            /*
            String data1 = txt1.getText().toString();
            String data2 = txt2.getText().toString();

            Intent i = new Intent(getApplicationContext(),Calculate.class);
            i.putExtra("num1",data1);
            i.putExtra("num2",data2);
            i.putExtra("flag","*");
            startActivityForResult(i,3);
            */
            // int result = Integer.parseInt(data1) * Integer.parseInt(data2);

            //txtresult.setText(String.valueOf(result));

        }else if(view.getId()==R.id.btndiv) {

            if(cal_flag==false) {

                txt1.setText(txt1.getText() + "/");
                cal_flag = true;
            }else{

                Toast.makeText(this,"Check input",Toast.LENGTH_LONG).show();
            }

            /*
            String data1 = txt1.getText().toString();
            String data2 = txt2.getText().toString();


            Intent i = new Intent(getApplicationContext(), Calculate.class);
            i.putExtra("num1", data1);
            i.putExtra("num2", data2);
            i.putExtra("flag", "/");
            startActivityForResult(i, 4);
            */
            //int result = Integer.parseInt(data1) / Integer.parseInt(data2);

            //txtresult.setText(String.valueOf(result));

        }else if(view.getId()==R.id.btn0){

            txt1.setText(txt1.getText()+"0");
            cal_flag=false;

        }else if(view.getId()==R.id.btn1){

            txt1.setText(txt1.getText()+"1");
            cal_flag=false;

        }else if(view.getId()==R.id.btn2){

            txt1.setText(txt1.getText()+"2");
            cal_flag=false;

        }else if(view.getId()==R.id.btn3){

            txt1.setText(txt1.getText()+"3");
            cal_flag=false;

        }else if(view.getId()==R.id.btn4){

            txt1.setText(txt1.getText()+"4");
            cal_flag=false;

        }else if(view.getId()==R.id.btn5){

            txt1.setText(txt1.getText()+"5");
            cal_flag=false;

        }else if(view.getId()==R.id.btn6){

            txt1.setText(txt1.getText()+"6");
            cal_flag=false;

        }else if(view.getId()==R.id.btn7){

            txt1.setText(txt1.getText()+"7");
            cal_flag=false;

        }else if(view.getId()==R.id.btn8){

            txt1.setText(txt1.getText()+"8");
            cal_flag=false;

        }else if(view.getId()==R.id.btn9){

            txt1.setText(txt1.getText()+"9");
            cal_flag=false;

        }else if(view.getId()==R.id.btnpoint){

            txt1.setText(txt1.getText()+".");
            cal_flag=true;

        }else if(view.getId()==R.id.btnequal){

            if(cal_flag==false){

                equal();

            }else{
                Toast.makeText(this,"Finish the inpur 1st",Toast.LENGTH_LONG).show();

            }

        }else if(view.getId()==R.id.btnac){

            txt1.setText("");
            txtresult.setText("");
        }else if(view.getId()==R.id.btnms){

            storeval();
        }else if(view.getId()==R.id.btnmr){

            getval();
        }else if(view.getId()==R.id.btnmplus){

            valplus();
        }else if(view.getId()==R.id.btnmminus){

            valminus();
        }else if(view.getId()==R.id.btnmc){

            cleanstore();
        }else if(view.getId()==R.id.btnclear) {

            String str = txt1.getText().toString();

            if(!str.isEmpty()){

                str = str.substring(0, str.length() - 1);
                ;
                txt1.setText(str.toString());

            }



        }


    }

    private void cleanstore(){


        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("result"); // will delete key name

        editor.clear();
        editor.commit(); // commit changes
        Toast.makeText(this,"Cleaned !!",Toast.LENGTH_LONG).show();

        getval();

    }

    private void storeval(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("result",txtresult.getText().toString()); // Storing string
        editor.commit(); // commit changes
        Toast.makeText(this,"Saved !!",Toast.LENGTH_LONG).show();



    }

    private void valplus(){


        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();

        String n = pref.getString("result","0");

        int p = Integer.parseInt(n)+1;

        editor.putString("result",String.valueOf(p)); // Storing string
        editor.commit(); // commit changes
        Toast.makeText(this,"Increased ++",Toast.LENGTH_SHORT).show();
        getval();


    }

    private void valminus(){


        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();

        String n = pref.getString("result","0");

        int p = Integer.parseInt(n)-1;

        editor.putString("result",String.valueOf(p)); // Storing string
        editor.commit(); // commit changes
        Toast.makeText(this,"decreased ++",Toast.LENGTH_SHORT).show();
        getval();


    }

    private void getval(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        txtresult.setText(pref.getString("result","not found"));
        Toast.makeText(this,"Data Showed !!",Toast.LENGTH_LONG).show();



    }


    private void store_all(){


        SharedPreferences pref = getApplicationContext().getSharedPreferences("ffw", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("result",txtresult.getText().toString()); // Storing string
        editor.putString("text1",txt1.getText().toString()); // Storing string

        editor.commit(); // commit changes



    }

    private void get_all(){


        SharedPreferences pref = getApplicationContext().getSharedPreferences("ffw", 0); // 0 - for private mode
        txtresult.setText(pref.getString("result","not found"));
        txt1.setText(pref.getString("text1","not found"));
        Toast.makeText(this,"Restored",Toast.LENGTH_SHORT).show();



    }



    private void equal(){

        char[] item = txt1.getText().toString().toCharArray();

       // Log.d("equal",p[0]);
       // Log.d("equal",p[1]);

       // Log.d("equal",txt1.getText().toString());

        if(txt1.getText().toString().contains("/")){

            String[] p = txt1.getText().toString().split("/");
           // Log.d("equal","true");

            if(p[1].equals("0")){

                txtresult.setText("INFINITY");
            }else{

                int res = Integer.parseInt(p[0])/Integer.parseInt(p[1]);
                txtresult.setText(String.valueOf(res));
            }

        }else if(txt1.getText().toString().contains("*")){

            // Log.d("equal","true");

                String[] p = txt1.getText().toString().split("\\*");
                int res = Integer.parseInt(p[0])*Integer.parseInt(p[1]);
                txtresult.setText(String.valueOf(res));


        }else if(txt1.getText().toString().contains("+")){

            // Log.d("equal","true");

            String[] p = txt1.getText().toString().split("\\+");
            int res = Integer.parseInt(p[0])+Integer.parseInt(p[1]);
            txtresult.setText(String.valueOf(res));


        }else if(txt1.getText().toString().contains("-")){

            // Log.d("equal","true");

            String[] p = txt1.getText().toString().split("\\-");
            int res = Integer.parseInt(p[0])-Integer.parseInt(p[1]);
            txtresult.setText(String.valueOf(res));


        }

        store_all();



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1 && resultCode == RESULT_OK){

            txtresult.setText(data.getStringExtra("ans"));

        }

        if(requestCode==2 && resultCode == RESULT_OK){

            txtresult.setText(data.getStringExtra("ans"));

        }

        if(requestCode==3 && resultCode == RESULT_OK){

            txtresult.setText(data.getStringExtra("ans"));

        }

        if(requestCode==4 && resultCode == RESULT_OK){

            txtresult.setText(data.getStringExtra("ans"));

        }
    }
    //...........................................



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            setContentView(R.layout.activity_main_land);

            buttonAction();
            get_all();



        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
            setContentView(R.layout.activity_main);


            buttonAction();
            get_all();



        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outstate){

        outstate.putString("val1",txt1.getText().toString());
        outstate.putString("val2",txt2.getText().toString());
        outstate.putString("valres",txtresult.getText().toString());



        super.onSaveInstanceState(outstate);





    }

    @Override
    protected void onRestoreInstanceState(Bundle instate){

        txt1.setText(instate.getString("val1").toString());
        txt2.setText(instate.getString("val2").toString());
        txtresult.setText(instate.getString("valres").toString());

        Toast.makeText(this,instate.getString("val1").toString(),Toast.LENGTH_LONG).show();


        super.onRestoreInstanceState(instate);



    }




    //...........................................



}
