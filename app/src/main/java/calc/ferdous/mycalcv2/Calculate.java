package calc.ferdous.mycalcv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Calculate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        Intent i = getIntent();

        if(i != null){

            String flag = i.getStringExtra("flag");
            String num1= i.getStringExtra("num1");
            String num2= i.getStringExtra("num2");

            if(flag.equals("+")){

                int result = Integer.parseInt(num1) + Integer.parseInt(num2);

                i.putExtra("ans",String.valueOf(result));
                setResult(Activity.RESULT_OK,i);
                finish();



            }

            if(flag.equals("-")){

                int result = Integer.parseInt(num1) - Integer.parseInt(num2);

                i.putExtra("ans",String.valueOf(result));
                setResult(Activity.RESULT_OK,i);
                finish();



            }

            if(flag.equals("*")){

                int result = Integer.parseInt(num1) * Integer.parseInt(num2);

                i.putExtra("ans",String.valueOf(result));
                setResult(Activity.RESULT_OK,i);
                finish();



            }

            if(flag.equals("/")){

                int result = Integer.parseInt(num1) / Integer.parseInt(num2);

                i.putExtra("ans",String.valueOf(result));
                setResult(Activity.RESULT_OK,i);
                finish();



            }



        }

    }
}
